﻿using System;
using NUnit.Framework;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Diagnostics.CodeAnalysis;
using System.Collections.Generic;

namespace TestExamples
{
    [TestFixture]
    public class Examples
    {
        [Test]
        public void BigInt()
        {
            var longVal = new System.Numerics.BigInteger(long.MaxValue);
            var largerThanLongVal = longVal * longVal;
        }

        [Test]
        public void Tuple()
        {
            var tup = new System.Tuple<int, string, DateTime>(1, "Brian", DateTime.Now);
            Debug.Write(tup.Item1 + tup.Item2 + tup.Item3);
        }

        [Test]
        public void CallerAttributes()
        {
            Method("Test");
        }

        public void Method(string outPut, [System.Runtime.CompilerServices.CallerMemberName] string name = "",
                   [System.Runtime.CompilerServices.CallerLineNumber] int line = -1,
                   [System.Runtime.CompilerServices.CallerFilePath] string path = "")
        {
            Debug.Write(outPut);
        }

        [Test]
        public void NamedParameters()
        {
            OutPut(message: "Test");
            OutPut(message: "Test", newLine:false);
        }

        public void OutPut(bool newLine = true, string message = "TestMessage", bool AddAmperSand = true)
        {
            Debug.Write(message);
            if (AddAmperSand) Debug.Write("&");
            if (newLine) Debug.Write(Environment.NewLine);
        }

        [Test]
        public void ExpandoObject()
        {
            dynamic sampleObject = new System.Dynamic.ExpandoObject();
            sampleObject.Test = "TestDynamic";
            sampleObject.InvokeExpandoObject = new Action(() => Console.Write(sampleObject.Test));
            sampleObject.InvokeExpandoObject.Invoke();
        }

        [Test]
        public void ParallelForeach()
        {
            var userList = new List<string>{ "Brian", "Steve", "Vince", "Paul", "John", "Eric", "Jeff"};
            System.Threading.Tasks.Parallel.ForEach(userList, usr => Debug.WriteLine(usr));
        }
    }
}